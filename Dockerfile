FROM alpine
RUN apk update && apk upgrade
RUN apk add suricata --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted
RUN apk add rsyslog
RUN apk add rsyslog-tls
RUN apk add openrc &&\
# Tell openrc its running inside a container, till now that has meant LXC
    sed -i 's/#rc_sys=""/rc_sys="lxc"/g' /etc/rc.conf &&\
# Tell openrc loopback and net are already there, since docker handles the networking
    echo 'rc_provide="loopback net"' >> /etc/rc.conf &&\
# no need for loggers
    sed -i 's/^#\(rc_logger="YES"\)$/\1/' /etc/rc.conf &&\
# can't get ttys unless you run the container in privileged mode
    sed -i '/tty/d' /etc/inittab &&\
# can't set hostname since docker sets it
    sed -i 's/hostname $opts/# hostname $opts/g' /etc/init.d/hostname &&\
# can't mount tmpfs since not privileged
    sed -i 's/mount -t tmpfs/# mount -t tmpfs/g' /lib/rc/sh/init.sh &&\
# can't do cgroups
    sed -i 's/cgroup_add_service /# cgroup_add_service /g' /lib/rc/sh/openrc-run.sh
RUN ln -s /etc/init.d/rsyslog /etc/runlevels/default/rsyslog
RUN ln -s /etc/init.d/suricata /etc/runlevels/default/suricata
RUN mkdir /etc/suricata/rules/custom
COPY suricata.yaml /etc/suricata/suricata.yaml
COPY custom.rules /etc/suricata/rules/custom/custom.rules
CMD ["/sbin/init"]
